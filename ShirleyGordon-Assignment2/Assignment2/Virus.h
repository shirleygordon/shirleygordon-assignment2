#pragma once
#include <string>
#include "Cell.h"

#define HALF 2

class Virus
{
private:
	std::string _RNA_sequence;

	// helper methods
	void RNA_to_DNA();

public:
	void init(const std::string RNA_sequence);
	void infect_cell(Cell& cell);
};