#include "Nucleus.h"

/*
Function initializes the gene fields.
Input: start, end, on_complementary_dna_strand.
Output: none.
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
Get functions return the values of the fields in the class.
Input: none.
Output: depends on parameter type.
*/
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

/*
Set functions change the value of the fields of the class.
Input: new value.
Output: none.
*/
void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}
void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
Function checks if a DNA sequence is valid (contains only the characters ATCG).
Input: dna sequence string.
Output: true if the string is valid, false otherwise.
*/
bool Nucleus::isValid(const std::string dna_sequence) const
{
	return dna_sequence.find_first_not_of(NUCLEOTIDES) == std::string::npos;
}

/*
Function converts the DNA sequence to its complementary strand.
Input: dna sequence string.
Output: the complementary DNA strand.
*/
std::string Nucleus::convert_to_complementary(const std::string dna_sequence) const
{
	unsigned int i = 0;
	std::string complementary_DNA_strand = dna_sequence; // copy the DNA sequence string into a new variable.

	// convert the DNA sequence to its complementary strand.
	for (i = 0; i < complementary_DNA_strand.length(); i++)
	{
		if (complementary_DNA_strand[i] == A_NUCLEOTIDE)
		{
			complementary_DNA_strand[i] = T_NUCLEOTIDE;
		}
		else if (complementary_DNA_strand[i] == T_NUCLEOTIDE)
		{
			complementary_DNA_strand[i] = A_NUCLEOTIDE;
		}
		else if (complementary_DNA_strand[i] == C_NUCLEOTIDE)
		{
			complementary_DNA_strand[i] = G_NUCLEOTIDE;
		}
		else if (complementary_DNA_strand[i] == G_NUCLEOTIDE)
		{
			complementary_DNA_strand[i] = C_NUCLEOTIDE;
		}
	}

	return complementary_DNA_strand;
}

/*
Function initializes the fields of the Nucleus class.
Input: dna sequence string.
Output: none.
*/
void Nucleus::init(const std::string dna_sequence)
{
	unsigned int i = 0;
	std::string complementary_DNA_strand = "";

	if (!isValid(dna_sequence)) // if the DNA sequence contains characters that aren't ATCG, the sequence is invalid.
	{
		std::cerr << "DNA sequence is invalid." << std::endl;
		_exit(1);
	}
	else
	{
		this->_DNA_strand = dna_sequence;
		this->_complementary_DNA_strand = convert_to_complementary(dna_sequence);
	}
}

/*
Function replaces all "T"s in a DNA sequence with "U"s
to convert it to RNA.
Input: reference of DNA sequence string.
Output: none.
*/
void Nucleus::DNA_to_RNA(std::string& DNA_sequence) const
{
	unsigned int i = 0;

	for (i = 0; i < DNA_sequence.length(); i++)
	{
		if (DNA_sequence[i] == T_NUCLEOTIDE)
		{
			DNA_sequence[i] = U_NUCLEOTIDE;
		}
	}
}

/*
Function generates RNA transcript from DNA strand, based on the gene.
Input: reference to gene object.
Output: RNA transcript string.
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	unsigned int i = 0;
	std::string RNA_transcript = "";
	
	// check which DNA strand the gene is on, and copy the its portion into the rna transcript string.
	if (gene.is_on_complementary_dna_strand())
	{
		RNA_transcript = this->_complementary_DNA_strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
	}
	else
	{
		RNA_transcript = this->_DNA_strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
	}

	// replace all occurrences of 'T' with 'U' in the RNA transcript.
	this->DNA_to_RNA(RNA_transcript);

	return RNA_transcript;
}

/*
Function returns the first DNA strand, reversed.
Input: none.
Output: reversed DNA strand string.
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed = this->_DNA_strand; // copy the first DNA strand into a new string.
	std::reverse(reversed.begin(), reversed.end()); // reverse it.
	return reversed;
}

/*
Function checks if a codon is in the DNA strand.
Input: codon string reference, the index to search from.
Output: true if the codon is in the DNA strand, false otherwise.
*/
bool Nucleus::is_codon_in_DNA_strand(const std::string& codon, int searchFrom) const
{
	return this->_DNA_strand.find(codon, searchFrom) != std::string::npos;
}

/*
Function returns the amount of times a certain codon appears in the first DNA strand in the Nucleus.
Input: the codon to search for.
Output: the amount of times it appears in the DNA strand.
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int searchFrom = 0; // variable represents the index in the string to start searching from.
	unsigned int count = 0;

	while (this->is_codon_in_DNA_strand(codon, searchFrom)) // while the codon is in the dna strand
	{
		searchFrom = this->_DNA_strand.find(codon, searchFrom); // try to find another occurrence of the substring in the dna strand.
		searchFrom += codon.length(); // the next search will begin from the index after the codon.
		count++;
	}

	return count;
}

/*
Function returns DNA strand string from Nucleus.
Input: none.
Output: DNA strand string.
*/
std::string Nucleus::get_DNA_strand() const
{
	return this->_DNA_strand;
}
