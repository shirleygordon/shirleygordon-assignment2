#pragma once
#include "Protein.h"

#define AMINO_ACID_CHAIN_LENGTH 7
#define MIN_GLUCOSE_LEVEL 50

class Mitochondrion
{
private:
	// fields
	unsigned int _glucose_level;
	bool _has_glucose_receptor;

public:
	// methods
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;
};