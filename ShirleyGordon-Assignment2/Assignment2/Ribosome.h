#pragma once
#include "Protein.h"
#include "AminoAcid.h"

#define CODON_LENGTH 3

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;
};