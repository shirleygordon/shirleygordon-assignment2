#include "Ribosome.h"

/*
Function creates a linked list of amino acids to form the protein.
Input: RNA transcript string.
Output: Pointer to head of protein linked list.
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* protein = new Protein;
	std::string codon = "";
	AminoAcid amino_acid;

	protein->init(); // initialize protein linked list

	while (RNA_transcript.length() >= CODON_LENGTH) // while the RNA transcript is longer or equal to 3, continue converting it to amino acids.
	{
		codon = RNA_transcript.substr(0, CODON_LENGTH); // generate the codon of the amino acid.
		amino_acid = get_amino_acid(codon);

		if (amino_acid == UNKNOWN) // invalid codon, stop creating protein and clear memory.
		{
			protein->clear();
			return nullptr;
		}
		else
		{
			protein->add(amino_acid); // add amino acid to protein linked list.
			RNA_transcript = RNA_transcript.substr(CODON_LENGTH); // shorten the RNA transcript by 3, to remove the codon that has already been converted to an amino acid.
		}
	}

	return protein;
}
