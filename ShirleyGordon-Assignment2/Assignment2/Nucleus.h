#pragma once
#include <string>
#include <iostream>
#define NUCLEOTIDES "ATCG"
#define A_NUCLEOTIDE 'A'
#define T_NUCLEOTIDE 'T'
#define C_NUCLEOTIDE 'C'
#define G_NUCLEOTIDE 'G'
#define U_NUCLEOTIDE 'U'

class Gene
{
private:
	// fields
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

public:
	// methods
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	
	// getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	// setters
	void setStart(const unsigned int start);
	void setEnd(const unsigned int end);
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
};

class Nucleus
{
private:
	// fields
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

	// helper methods
	std::string convert_to_complementary(const std::string dna_sequence) const;
	bool isValid(const std::string dna_sequence) const;
	bool is_codon_in_DNA_strand(const std::string& codon, int lastFoundPos) const;
	void DNA_to_RNA(std::string& DNA_sequence) const;

public:
	// methods
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
	std::string get_DNA_strand() const;
};