#include "Virus.h"

/*
Function initializes virus RNA sequence.
Input: RNA sequence string.
Output: none.
*/
void Virus::init(const std::string RNA_sequence)
{
	this->_RNA_sequence = RNA_sequence;
}

/*
Function replaces all occurrences of 'U' with 'T' in the RNA transcript.
Input: none.
Output: none.
*/
void Virus::RNA_to_DNA()
{
	unsigned int i = 0;

	for (i = 0; i < this->_RNA_sequence.length(); i++)
	{
		if (this->_RNA_sequence[i] == U_NUCLEOTIDE)
		{
			this->_RNA_sequence[i] = T_NUCLEOTIDE;
		}
	}
}

/*
Function infects a DNA sequence with the virus,
in the middle of the DNA sequence.
Include: Cell reference.
Output: none.
*/
void Virus::infect_cell(Cell& cell)
{
	unsigned int i = 0;
	std::string DNA_sequence = "";
	std::string DNA_start = "";
	Nucleus nucleus;

	this->RNA_to_DNA(); // convert RNA sequence to DNA.

	nucleus = cell.get_nucleus(); // get pointer to Nucleus from cell
	DNA_sequence = nucleus.get_DNA_strand(); // get DNA sequence from Nucleus.

	// create the new, infected DNA sequence
	DNA_sequence = DNA_sequence.substr(0, DNA_sequence.length() / HALF) +
				   this->_RNA_sequence +
				   DNA_sequence.substr(DNA_sequence.length() / HALF);

	// initialize Nucleus with new, infected DNA sequence.
	nucleus.init(DNA_sequence);
}
