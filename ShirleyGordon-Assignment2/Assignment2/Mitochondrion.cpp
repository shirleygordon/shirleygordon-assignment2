#include "Mitochondrion.h"

/*
Function initializes the fields of a Mitochondrion.
Input: none.
Output: none.
*/
void Mitochondrion::init()
{
	this->_glucose_level = 0;
	this->_has_glucose_receptor = false;
}

/*
Function changes the _has_glucose_receptor to tru
if the protein passed to it matches the required configuration.
Input: reference to protein linked list.
Output: none.
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	unsigned int i = 0;
	bool matches = true; // true while the linked list matches the required configuration.
	AminoAcid amino_acid_chain[] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END }; // required configuration
	Protein temp = protein;

	// check if the protein matches the required configuration
	for (i = 0; i < AMINO_ACID_CHAIN_LENGTH && matches; i++)
	{
		if (temp.get_first()->get_data() != amino_acid_chain[i]) // if a node doesn't match, it means the chain is invalid.
		{
			matches = false;
		}
		else
		{
			temp.set_first(temp.get_first()->get_next()); // set the next node to be the first node.
		}
	}

	if (matches) // if the protein matches the required configuration, set _has_glucose_receptor to true.
	{
		this->_has_glucose_receptor = true;
	}
}

/*
Function sets the glucose level.
Input: glucose units.
Output: none.
*/
void Mitochondrion::set_glucose(const unsigned int glucose_units)
{
	this->_glucose_level = glucose_units;
}

/*
Function checks if the Mitochondrion can produce ATP.
Input: none.
Output: true if it can produce ATP, false otherwise.
*/
bool Mitochondrion::produceATP() const
{
	return this->_has_glucose_receptor == true && this->_glucose_level >= MIN_GLUCOSE_LEVEL;
}

