#include "Cell.h"

/*
Function initializes the Cell fields.
Input: dna sequence, glucose receptor gene.
Output: none.
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocuse_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_atp_units = 0;
}

/*
Function tries to produce ATP using the different organelles.
Input: none.
Output: true if ATP was successfully produced, false otherwise.
*/
bool Cell::get_ATP()
{
	std::string RNA_transcript = "";
	Protein* protein = new Protein;
	bool success = false;

	// get RNA transcript from Nucleus
	RNA_transcript = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);
	
	// create protein in the Ribosome
	protein = this->_ribosome.create_protein(RNA_transcript);

	if (protein == nullptr) // if protein couldn't be created, exit the program.
	{
		std::cerr << "Unable to create protein." << std::endl;
		_exit(1);
	}
	else // if protein was created successfully
	{
		this->_mitochondrion.insert_glucose_receptor(*protein);
		this->_mitochondrion.set_glucose(MIN_GLUCOSE_LEVEL); // at lease 50 glucose units are required to produce ATP.
		if (this->_mitochondrion.produceATP()) // if ATP was successfully produced
		{
			this->_atp_units = ATP;
			success = true;
		}
	}

	return success;
}

/*
Function returns Nucleus pointer from Cell.
Input: none.
Output: pointer to Nucleus object.
*/
Nucleus& Cell::get_nucleus()
{
	return this->_nucleus;
}